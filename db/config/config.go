package config

import (
	"github.com/joho/godotenv"
	"os"
	"strconv"
)

type DatabaseConnectionConfig struct {
	Host     string
	Port     int
	User     string
	Password string
	DbName   string
}

func New() *DatabaseConnectionConfig {
	err := godotenv.Load()

	if err != nil {
		panic("Не удается загрузить переменные окружения")
	}

	return &DatabaseConnectionConfig{
		Host:     host(),
		Port:     port(),
		User:     user(),
		Password: password(),
		DbName:   dbname(),
	}
}

func host() string {
	return os.Getenv("DB_HOST")
}

func port() int {
	port, err := strconv.Atoi(os.Getenv("DB_PORT"))

	if err != nil {
		panic("Не удается получить порт для подключения к БД!")
	}

	return port
}

func user() string {
	return os.Getenv("DB_USER")
}

func password() string {
	return os.Getenv("DB_PASSWORD")
}

func dbname() string {
	return os.Getenv("DB_NAME")

}
