package db

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"skbt-pokur-bot/db/config"
	"skbt-pokur-bot/internal/core/domain"
)

func New() *gorm.DB {
	dbConConf := config.New()

	dsn := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		dbConConf.Host,
		dbConConf.Port,
		dbConConf.User,
		dbConConf.Password,
		dbConConf.DbName,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Не удалось установить подключение к БД!")
	}

	err = db.AutoMigrate(&domain.User{}, &domain.Chat{}, &domain.UserChat{}, &domain.Remind{}, &domain.RemindSubscription{})
	if err != nil {
		panic(err)
	}

	fmt.Println("Подключение к БД установлено!")

	return db
}
