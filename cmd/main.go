package main

import (
	"skbt-pokur-bot/internal/core/bot"
)

func main() {
	b := bot.New()

	b.Start()
}
