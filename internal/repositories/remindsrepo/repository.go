package remindsrepo

import (
	"context"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"skbt-pokur-bot/internal/core/domain"
	"time"
)

type remindsRepository struct {
	ctx context.Context
	db  *gorm.DB
}

func New(ctx context.Context, db *gorm.DB) *remindsRepository {
	return &remindsRepository{
		ctx: ctx,
		db:  db,
	}
}

func (repo *remindsRepository) Get(id int64) (*domain.Remind, error) {
	remind := domain.Remind{
		ID: id,
	}

	res := repo.db.First(&remind)

	if res.Error != nil {
		msg := fmt.Sprintf("[RemindsRepository] query error: %v\n", res.Error)
		return nil, errors.New(msg)
	}

	return &remind, nil
}

func (repo *remindsRepository) Save(remind *domain.Remind) error {
	res := repo.db.Create(&remind)

	if res.Error != nil {
		msg := fmt.Sprintf("[RemindsRepository] query error: %v\n", res.Error)
		return errors.New(msg)
	}

	return nil
}

func (repo *remindsRepository) Remove(id int64) error {
	res := repo.db.Delete(&domain.Remind{ID: id})

	if res.Error != nil {
		msg := fmt.Sprintf("[RemindsRepository] cannot delete remind with id = %d", id)
		return errors.New(msg)
	}

	return nil
}

func (repo *remindsRepository) SubscribeUser(remindId, chatId, userId int64) error {
	subscr := domain.RemindSubscription{
		UserID:   userId,
		RemindID: remindId,
		ChatID:   chatId,
		IsActive: true,
	}

	res := repo.db.Create(&subscr)

	if res.Error != nil {
		msg := fmt.Sprintf("[RemindsRepository] query error: %v\n", res.Error)
		return errors.New(msg)
	}

	return nil
}

func (repo *remindsRepository) All(chat *domain.Chat, date time.Time) ([]*domain.Remind, error) {
	y, m, d := date.Date()

	from := time.Date(y, m, d, 0, 0, 0, 0, date.Location())
	to := from.Add(time.Hour * 24)

	var reminds []*domain.Remind

	res := repo.db.
		Where("chat_id = ?", chat.ID).
		Where("time >= ?", from).
		Where("time <= ?", to).
		Find(&reminds)

	if res.Error != nil {
		msg := fmt.Sprintf("[RemindsRepository] query error: %v\n", res.Error)
		return nil, errors.New(msg)
	}

	return reminds, nil
}
