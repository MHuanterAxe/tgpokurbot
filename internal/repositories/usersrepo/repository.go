package usersrepo

import (
	"context"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"skbt-pokur-bot/internal/core/domain"
)

type usersRepository struct {
	ctx context.Context
	db  *gorm.DB
}

func New(ctx context.Context, db *gorm.DB) *usersRepository {
	return &usersRepository{
		ctx: ctx,
		db:  db,
	}
}

func (repo *usersRepository) Get(id int64) (*domain.User, error) {
	user := domain.User{
		TgUserID: id,
	}

	res := repo.db.First(&user)

	if res.Error != nil {
		msg := fmt.Sprintf("[UsersRepository] query error: %v\n", res.Error)
		return nil, errors.New(msg)
	}

	return &user, nil
}

func (repo *usersRepository) Exists(id int64) (bool, error) {
	user := domain.User{}

	res := repo.db.Where("tg_user_id = ?", id).First(&user)

	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		return false, nil
	}

	if res.Error != nil {
		msg := fmt.Sprintf("[UsersRepository] query error: %v\n", res.Error)
		return false, errors.New(msg)
	}

	return true, nil
}

func (repo *usersRepository) Save(user *domain.User) error {
	res := repo.db.Create(&user)

	if res.Error != nil {
		msg := fmt.Sprintf("[UsersRepository] query error: %v\n", res.Error)
		return errors.New(msg)
	}

	return nil
}

func (repo *usersRepository) AddToChat(user *domain.User, chat *domain.Chat) error {
	err := repo.db.Model(&chat).Association("Users").Append(&user)

	if err != nil {
		msg := fmt.Sprintf("[ChatsRepository] query error: %v\n", err)
		return errors.New(msg)
	}

	return nil
}

func (repo *usersRepository) ExistsInChat(user *domain.User, chat *domain.Chat) (bool, error) {
	err := repo.db.Model(&chat).Association("Users").Find(&user)

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return false, nil
	}

	if err != nil {
		msg := fmt.Sprintf("[UsersRepository] query error: %v\n", err)
		return false, errors.New(msg)
	}

	return true, nil
}
