package chatsrepo

import (
	"context"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"skbt-pokur-bot/internal/core/domain"
	"time"
)

type chatsRepository struct {
	ctx context.Context
	db  *gorm.DB
}

func New(ctx context.Context, db *gorm.DB) *chatsRepository {
	return &chatsRepository{
		ctx: ctx,
		db:  db,
	}
}

func (repo *chatsRepository) Get(id int64) (*domain.Chat, error) {
	chat := domain.Chat{
		TgChatID: id,
	}

	res := repo.db.First(&chat)

	if res.Error != nil {
		msg := fmt.Sprintf("[ChatsRepository] query error: %v\n", res.Error)
		return nil, errors.New(msg)
	}

	return &chat, nil
}

func (repo *chatsRepository) Exists(id int64) (bool, error) {
	chat := domain.Chat{}

	res := repo.db.Where("tg_chat_id = ?", id).First(&chat)

	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		return false, nil
	}

	if res.Error != nil {
		msg := fmt.Sprintf("[ChatsRepository] query error: %v\n", res.Error)
		return false, errors.New(msg)
	}

	return true, nil
}

func (repo *chatsRepository) GetUserAdded(id int64) (*domain.User, error) {
	chat, err := repo.Get(id)

	if err != nil {
		msg := fmt.Sprintf("[ChatsRepository] cannot get user from chat with id = %d", id)
		return nil, errors.New(msg)
	}

	return &chat.UserAdded, nil
}

func (repo *chatsRepository) Save(chat *domain.Chat) error {
	res := repo.db.Create(&chat)

	if res.Error != nil {
		msg := fmt.Sprintf("[ChatsRepository] query error: %v\n", res.Error)
		return errors.New(msg)
	}

	return nil
}

func (repo *chatsRepository) AddUser(chat *domain.Chat, user *domain.User) error {
	userInChat := domain.UserChat{
		UserID:    user.ID,
		ChatID:    chat.ID,
		CreatedAt: time.Now(),
	}

	res := repo.db.Create(&userInChat)

	if res.Error != nil {
		msg := fmt.Sprintf("[ChatsRepository] query error: %v\n", res.Error)
		return errors.New(msg)
	}

	return nil
}

func (repo *chatsRepository) HasUser(chat *domain.Chat, user *domain.User) (bool, error) {
	var userInChat domain.UserChat

	res := repo.db.
		Where("chat_id = ?", chat.ID).
		Where("user_id = ?", user.ID).
		First(&userInChat)

	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		return false, nil
	}

	if res.Error != nil {
		msg := fmt.Sprintf("[ChatsRepository] query e$rror: %v\n", res.Error)
		return false, errors.New(msg)
	}

	return true, nil
}

func (repo *chatsRepository) DeleteUser(chat *domain.Chat, user *domain.User) error {
	var userInChat domain.UserChat

	res := repo.db.
		Where("chat_id = ?", chat.ID).
		Where("user_id = ?", user.ID).
		Delete(&userInChat)

	if res.Error != nil {
		msg := fmt.Sprintf("[ChatsRepository] query error: %v\n", res.Error)
		return errors.New(msg)
	}

	return nil
}
