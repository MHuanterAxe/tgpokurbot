package domain

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

type Chat struct {
	ID          int64  `gorm:"primaryKey"`
	UserAddedID int64  `gorm:"unique"`
	TgChatID    int64  `gorm:"unique"`
	UserAdded   User   `gorm:"foreignKey:UserAddedID;references:TgUserID"`
	Users       []User `gorm:"many2many:user_chats;"`
}

func TelegramChat(chat *tgbotapi.Chat) *Chat {
	if chat == nil {
		return nil
	}

	return &Chat{
		TgChatID: chat.ID,
	}
}
