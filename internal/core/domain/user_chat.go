package domain

import (
	"gorm.io/gorm"
	"time"
)

type UserChat struct {
	UserID    int64     `gorm:"primaryKey"`
	ChatID    int64     `gorm:"primaryKey"`
	CreatedAt time.Time `gorm:"primaryKey;autoCreateTime"`
	DeletedAt gorm.DeletedAt
}
