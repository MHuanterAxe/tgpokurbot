package domain

import "time"

type RemindSubscription struct {
	ID        int64 `gorm:"primaryKey"`
	UserID    int64
	RemindID  int64
	ChatID    int64
	CreatedAt time.Time
	IsActive  bool `gorm:"default:false"`
	User      User
	Chat      Chat
	Remind    Remind
}
