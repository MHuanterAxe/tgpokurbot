package domain

import "time"

type Remind struct {
	ID          int64 `gorm:"primaryKey"`
	ChatID      int64
	CreatorID   int64
	CreatedAt   time.Time `gorm:"autoCreateTime"`
	Time        time.Time
	Chat        Chat
	Creator     User
	Subscribers []User `gorm:"many2many:remind_subscriptions;"`
}
