package domain

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

type User struct {
	ID        int64 `gorm:"primaryKey"`
	TgUserID  int64 `gorm:"unique"`
	FirstName string
	LastName  string
	UserName  string `gorm:"unique"`
}

func TelegramUser(user *tgbotapi.User) *User {
	if user == nil {
		return nil
	}

	return &User{
		TgUserID:  user.ID,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		UserName:  user.UserName,
	}
}
