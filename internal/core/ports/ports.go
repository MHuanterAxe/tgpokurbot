package ports

import (
	"skbt-pokur-bot/internal/core/domain"
	"time"
)

type UsersRepository interface {
	Get(id int64) (*domain.User, error)
	Exists(id int64) (bool, error)
	Save(*domain.User) error
}

type UsersService interface {
	Get(id int64) (*domain.User, error)
	Create(tgUserId int64, firstName, lastName, userName string) (*domain.User, error)
	GetOrCreate(user *domain.User) (*domain.User, error)
}

type ChatsRepository interface {
	Get(id int64) (*domain.Chat, error)
	Exists(id int64) (bool, error)
	GetUserAdded(id int64) (*domain.User, error)
	AddUser(chat *domain.Chat, user *domain.User) error
	HasUser(chat *domain.Chat, user *domain.User) (bool, error)
	DeleteUser(chat *domain.Chat, user *domain.User) error
	Save(*domain.Chat) error
}

type ChatsService interface {
	Get(id int64) (*domain.Chat, error)
	GetUserAdded(chatId int64) (*domain.User, error)
	Create(userId, chatId int64) (*domain.Chat, error)
	GetOrCreate(chat *domain.Chat) (*domain.Chat, error)
	AddUser(user *domain.User, chat *domain.Chat) (*domain.Chat, error)
	HasUser(chat *domain.Chat, user *domain.User) (bool, error)
	DeleteUser(usr *domain.User, chat *domain.Chat) (*domain.Chat, error)
}

type RemindsRepository interface {
	Get(id int64) (*domain.Remind, error)
	All(chat *domain.Chat, date time.Time) ([]*domain.Remind, error)
	Save(*domain.Remind) error
	Remove(id int64) error
	SubscribeUser(id, chatId, userId int64) error
}

type RemindsService interface {
	Get(id int64) (*domain.Remind, error)
	All(chat *domain.Chat, date time.Time) ([]*domain.Remind, error)
	Create(chatId, creatorId int64, time time.Time) (*domain.Remind, error)
	Subscribe(chatId, remindId, userId int64) (*domain.Remind, error)
}
