package services

import (
	"context"
	"gorm.io/gorm"
	"skbt-pokur-bot/internal/core/services/chatsrv"
	"skbt-pokur-bot/internal/core/services/remindsrv"
	"skbt-pokur-bot/internal/core/services/usersrv"
	"skbt-pokur-bot/internal/repositories/chatsrepo"
	"skbt-pokur-bot/internal/repositories/remindsrepo"
	"skbt-pokur-bot/internal/repositories/usersrepo"
)

type Services struct {
	Users   *usersrv.Service
	Chats   *chatsrv.Service
	Reminds *remindsrv.Service
}

func New(ctx context.Context, db *gorm.DB) *Services {
	usersRepository := usersrepo.New(ctx, db)
	chatsRepository := chatsrepo.New(ctx, db)
	remindsRepository := remindsrepo.New(ctx, db)

	usersService := usersrv.New(usersRepository)
	chatsService := chatsrv.New(chatsRepository, usersService)
	remindsService := remindsrv.New(remindsRepository, chatsRepository)

	return &Services{
		Users:   usersService,
		Chats:   chatsService,
		Reminds: remindsService,
	}
}
