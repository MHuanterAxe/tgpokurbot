package usersrv

import (
	"fmt"
	"skbt-pokur-bot/internal/core/domain"
	"skbt-pokur-bot/internal/core/ports"
)

type Service struct {
	usersRepository ports.UsersRepository
}

func New(usersRepository ports.UsersRepository) *Service {
	return &Service{
		usersRepository: usersRepository,
	}
}

func (srv *Service) Get(id int64) (*domain.User, error) {
	user, err := srv.usersRepository.Get(id)

	if err != nil {
		return nil, getErr(err, id)
	}

	return user, nil
}

func (srv *Service) Create(tgUserId int64, firstName, lastName, userName string) (*domain.User, error) {
	user := domain.User{
		TgUserID:  tgUserId,
		FirstName: firstName,
		LastName:  lastName,
		UserName:  userName,
	}

	err := srv.usersRepository.Save(&user)

	if err != nil {
		return nil, createErr(err, user.TgUserID, user.UserName)
	}

	return &user, nil
}

func (srv *Service) GetOrCreate(user *domain.User) (*domain.User, error) {
	exists, err := srv.usersRepository.Exists(user.TgUserID)

	if err != nil {
		return nil, getErr(err, user.TgUserID)
	}

	if exists {
		return srv.Get(user.TgUserID)
	}

	return srv.Create(user.TgUserID, user.FirstName, user.LastName, user.UserName)
}

func getErr(err error, id int64) error {
	return fmt.Errorf("[Get] Невозможно получить пользователя c id = %d: %w\n", id, err)
}

func createErr(err error, id int64, userName string) error {
	return fmt.Errorf("[Create] Невозможно создать пользователя %s c id = %d: %w\n", userName, id, err)
}
