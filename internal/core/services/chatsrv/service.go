package chatsrv

import (
	"fmt"
	"skbt-pokur-bot/internal/core/domain"
	"skbt-pokur-bot/internal/core/ports"
)

type Service struct {
	chatsRepository ports.ChatsRepository
	usersService    ports.UsersService
}

func New(chatsRepository ports.ChatsRepository, usersService ports.UsersService) *Service {
	return &Service{
		chatsRepository: chatsRepository,
		usersService:    usersService,
	}
}

func (srv *Service) Get(id int64) (*domain.Chat, error) {
	chat, err := srv.chatsRepository.Get(id)

	if err != nil {
		return nil, getErr(err, id)
	}

	return chat, nil
}

func (srv *Service) GetUserAdded(chatId int64) (*domain.User, error) {
	user, err := srv.chatsRepository.GetUserAdded(chatId)

	if err != nil {
		return nil, getErr(err, chatId)
	}

	return user, nil
}

func (srv *Service) Create(userId, chatId int64) (*domain.Chat, error) {
	chat := domain.Chat{
		UserAddedID: userId,
		TgChatID:    chatId,
	}

	err := srv.chatsRepository.Save(&chat)

	if err != nil {
		return nil, createErr(err, userId, chatId)
	}

	return &chat, nil
}

func (srv *Service) GetOrCreate(chat *domain.Chat) (*domain.Chat, error) {
	exists, err := srv.chatsRepository.Exists(chat.TgChatID)

	if err != nil {
		return nil, getErr(err, chat.TgChatID)
	}

	if exists {
		return srv.Get(chat.TgChatID)
	}

	return srv.Create(chat.UserAddedID, chat.TgChatID)
}

func (srv *Service) AddUser(user *domain.User, chat *domain.Chat) (*domain.Chat, error) {
	exists, err := srv.chatsRepository.HasUser(chat, user)

	if err != nil {
		return nil, addUserErr(err, user.TgUserID, chat.TgChatID)
	}

	if exists {
		return srv.Get(chat.TgChatID)
	}

	err = srv.chatsRepository.AddUser(chat, user)

	if err != nil {
		return nil, addUserErr(err, user.TgUserID, chat.TgChatID)
	}

	return srv.Get(chat.TgChatID)
}

func (srv *Service) HasUser(chat *domain.Chat, user *domain.User) (bool, error) {
	exists, err := srv.chatsRepository.HasUser(chat, user)

	if err != nil {
		return false, hasUserErr(err, user.TgUserID, chat.TgChatID)
	}

	return exists, nil
}

func (srv *Service) DeleteUser(user *domain.User, chat *domain.Chat) (*domain.Chat, error) {
	err := srv.chatsRepository.DeleteUser(chat, user)

	if err != nil {
		return nil, deleteUserErr(err, user.TgUserID, chat.TgChatID)
	}

	return chat, nil
}

func getErr(err error, id int64) error {
	return fmt.Errorf("[Get] Невозможно получить чат c id = %d: %w\n", id, err)
}

func createErr(err error, userId, chatId int64) error {
	return fmt.Errorf("[Create] Невозможно создать чат c id = %d с пользователем с id = %d: %w\n", chatId, userId, err)
}

func addUserErr(err error, userId, chatId int64) error {
	return fmt.Errorf("[AddUser] Невозможно добавить пользователя с id = %d в чат c id = %d: %w\n", userId, chatId, err)
}

func hasUserErr(err error, userId, chatId int64) error {
	return fmt.Errorf("[HasUser] Невозможно определить пользователя с id = %d в чате c id = %d: %w\n", userId, chatId, err)
}

func deleteUserErr(err error, userId, chatId int64) error {
	return fmt.Errorf("[DeleteUser] Невозможно удалить пользователя с id = %d из чата c id = %d: %w\n", userId, chatId, err)
}
