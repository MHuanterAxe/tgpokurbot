package remindsrv

import (
	"errors"
	"fmt"
	"skbt-pokur-bot/internal/core/domain"
	"skbt-pokur-bot/internal/core/ports"
	"time"
)

type Service struct {
	remindsRepository ports.RemindsRepository
	chatsRepository   ports.ChatsRepository
}

func New(remindsRepository ports.RemindsRepository, chatsRepository ports.ChatsRepository) *Service {
	return &Service{
		remindsRepository: remindsRepository,
		chatsRepository:   chatsRepository,
	}
}

func (srv *Service) Get(id int64) (*domain.Remind, error) {
	remind, err := srv.remindsRepository.Get(id)

	if err != nil {
		return nil, getErr(err, id)
	}

	return remind, nil
}

func (srv *Service) Create(chatId, creatorId int64, time time.Time) (*domain.Remind, error) {
	remind := domain.Remind{
		ChatID:    chatId,
		CreatorID: creatorId,
		Time:      time,
	}

	err := srv.remindsRepository.Save(&remind)

	if err != nil {
		return nil, createErr(err, creatorId, chatId)
	}

	return &remind, nil
}

func (srv *Service) Subscribe(remindId, chatId, userId int64) (*domain.Remind, error) {
	err := srv.remindsRepository.SubscribeUser(remindId, chatId, userId)

	if err != nil {
		return nil, subscribeErr(remindId, chatId, userId)
	}

	remind, err := srv.Get(remindId)

	if err != nil {
		return nil, err
	}

	return remind, nil
}

func (srv *Service) All(chat *domain.Chat, date time.Time) ([]*domain.Remind, error) {
	c, err := srv.chatsRepository.Get(chat.TgChatID)

	if err != nil {
		return nil, err
	}

	reminds, err := srv.remindsRepository.All(c, date)

	if err != nil {
		return nil, err
	}

	return reminds, nil
}

func getErr(err error, id int64) error {
	return fmt.Errorf("[Get] Невозможно получить уведомление c id = %d: %w\n", id, err)
}

func createErr(err error, creatorId, chatId int64) error {
	return fmt.Errorf("[Create] Невозможно создать уведомление пользователем с id = %d в чате с id = %d: %w\n", creatorId, chatId, err)
}

func subscribeErr(remindId, chatId, userId int64) error {
	msg := fmt.Sprintf("cannot subscribe to remind with id  = %d, userId = %d and chatId = %d", remindId, userId, chatId)
	return errors.New(msg)
}
