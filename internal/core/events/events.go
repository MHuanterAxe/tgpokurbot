package events

import (
	"context"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"skbt-pokur-bot/internal/core/domain"
	"skbt-pokur-bot/internal/core/events/types"
)

type Event struct {
	Type types.EventType
	User *domain.User
	Chat *domain.Chat
	Meta interface{}
}

type EventProcessor interface {
	Process(event Event) error
}

type EventTransformer interface {
	Transform(update tgbotapi.Update) (Event, error)
}

type EventListener interface {
	Listen(ctx context.Context, updChan tgbotapi.UpdatesChannel) error
}

type EventHandler interface {
	Handle(event Event) error
}
