package event_transformer

import (
	"errors"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"skbt-pokur-bot/internal/core/commands"
	"skbt-pokur-bot/internal/core/domain"
	"skbt-pokur-bot/internal/core/events"
	"skbt-pokur-bot/internal/core/events/types"
)

type eventTransformer struct {
	processor events.EventProcessor
}

func New(processor events.EventProcessor) *eventTransformer {
	return &eventTransformer{
		processor: processor,
	}
}

func (t *eventTransformer) Transform(update tgbotapi.Update) (events.Event, error) {
	evPtr, err := t.parse(update)

	if err != nil {
		err = errors.New("cannot parse update to event")

		return t.unknownType(&update), err
	}

	if evPtr != nil {
		return *evPtr, nil
	}

	return t.unknownType(&update), nil
}

func (t *eventTransformer) parse(update tgbotapi.Update) (*events.Event, error) {
	ev := t.hasMessage(&update)

	if ev != nil {
		return ev, nil
	}

	ev = t.trySubscribeForReminds(&update)

	if ev != nil {
		return ev, nil
	}

	ev = t.tryUnsubscribeForReminds(&update)

	if ev != nil {
		return ev, nil
	}

	ev = t.tryGetSchedules(&update)

	if ev != nil {
		return ev, nil
	}

	ev = t.tryCreateUnscheduledRemind(&update)

	if ev != nil {
		return ev, nil
	}

	return ev, nil
}

func (t *eventTransformer) trySubscribeForReminds(update *tgbotapi.Update) *events.Event {
	if update.Message.IsCommand() && update.Message.Command() == string(commands.SubscribeForRemindsInChat) {
		return &events.Event{
			Type: types.UserSubscribedForReminds,
			User: domain.TelegramUser(update.Message.From),
			Chat: &domain.Chat{
				TgChatID:    update.Message.Chat.ID,
				UserAddedID: update.Message.From.ID,
			},
		}
	}

	return nil
}

func (t *eventTransformer) tryUnsubscribeForReminds(update *tgbotapi.Update) *events.Event {
	if update.Message.IsCommand() && update.Message.Command() == string(commands.UnsubscribeForRemindsInChat) {
		return &events.Event{
			Type: types.UserUnsubscribedForReminds,
			User: domain.TelegramUser(update.Message.From),
			Chat: &domain.Chat{
				TgChatID:    update.Message.Chat.ID,
				UserAddedID: update.Message.From.ID,
			},
		}
	}

	return nil
}

func (t *eventTransformer) tryGetSchedules(update *tgbotapi.Update) *events.Event {
	if update.Message.Text == "Предстоящие покуры" {
		return &events.Event{
			Type: types.ChatSchedules,
			User: domain.TelegramUser(update.Message.From),
			Chat: domain.TelegramChat(update.Message.Chat),
		}
	}

	return nil
}

func (t *eventTransformer) tryCreateUnscheduledRemind(update *tgbotapi.Update) *events.Event {
	if update.Message.Text == "Создать напоминание" {
		return &events.Event{
			Type: types.CreateUnscheduledRemind,
			User: domain.TelegramUser(update.Message.From),
			Chat: domain.TelegramChat(update.Message.Chat),
		}
	}

	return nil
}

func (t *eventTransformer) hasMessage(update *tgbotapi.Update) *events.Event {
	if update.Message == nil {
		tp := t.unknownType(update)
		return &tp
	}

	return nil
}

func (t *eventTransformer) unknownType(update *tgbotapi.Update) events.Event {
	return events.Event{
		Type: types.Unknown,
		Chat: domain.TelegramChat(update.Message.Chat),
	}
}
