package event_processor

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"skbt-pokur-bot/internal/core/events"
	"skbt-pokur-bot/internal/core/events/handlers"
	"skbt-pokur-bot/internal/core/events/types"
	"skbt-pokur-bot/internal/core/services"
)

type eventProcessor struct {
	handlers *handlers.Handlers
}

func New(api *tgbotapi.BotAPI, services *services.Services) *eventProcessor {
	h := handlers.New(api, services)

	return &eventProcessor{
		handlers: h,
	}
}

func (p *eventProcessor) Process(event events.Event) error {
	switch event.Type {
	case types.Unknown:
		return p.handlers.Unknown.Handle(event)
	case types.UserSubscribedForReminds:
		return p.handlers.UserSubscribedForReminds.Handle(event)
	case types.UserUnsubscribedForReminds:
		return p.handlers.UserUnsubscribedForReminds.Handle(event)
	case types.ChatSchedules:
		return p.handlers.ChatSchedules.Handle(event)
	case types.CreateUnscheduledRemind:
		return p.handlers.CreateUnscheduledRemind.Handle(event)
	}

	return nil
}
