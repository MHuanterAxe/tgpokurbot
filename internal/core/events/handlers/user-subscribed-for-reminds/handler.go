package user_subscribed_for_reminds

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
	"skbt-pokur-bot/internal/core/domain"
	"skbt-pokur-bot/internal/core/events"
	"skbt-pokur-bot/internal/core/keyboards"
	"skbt-pokur-bot/internal/core/services"
)

type Handler struct {
	api      *tgbotapi.BotAPI
	services *services.Services
}

func New(api *tgbotapi.BotAPI, services *services.Services) *Handler {
	return &Handler{
		api:      api,
		services: services,
	}
}

func (h *Handler) Handle(event events.Event) error {
	usr, err := h.services.Users.GetOrCreate(event.User)
	if err != nil {
		return err
	}

	chat, err := h.services.Chats.GetOrCreate(event.Chat)
	if err != nil {
		return err
	}

	hasUser, err := h.services.Chats.HasUser(chat, usr)

	if err != nil {
		return err
	}

	if hasUser {
		return h.hasUser(chat, usr)
	}

	return h.addUser(chat, usr)
}

func (h *Handler) addUser(chat *domain.Chat, usr *domain.User) error {
	chat, err := h.services.Chats.AddUser(usr, chat)
	if err != nil {
		return err
	}

	log.Printf("Пользователь %s присоединился к чату %d!", usr.UserName, chat.TgChatID)

	text := fmt.Sprintf("%s включил уведомления в чате.\nТеперь он может учавствовать в покурах", usr.FirstName)

	msg := tgbotapi.NewMessage(chat.TgChatID, text)

	msg.ReplyMarkup = keyboards.BaseKeyboard()

	_, err = h.api.Send(msg)
	if err != nil {
		return err
	}

	return nil
}

func (h *Handler) hasUser(chat *domain.Chat, usr *domain.User) error {
	log.Printf("Пользователь %s уже есть в чате %d!", usr.UserName, chat.TgChatID)

	text := fmt.Sprintf("%s, ты уже активировал уведомления в чате.\nЧтобы деактивировать их воспользуйся командой /unsubscribe", usr.FirstName)

	msg := tgbotapi.NewMessage(chat.TgChatID, text)

	msg.ReplyMarkup = keyboards.BaseKeyboard()

	_, err := h.api.Send(msg)
	if err != nil {
		return err
	}

	return nil
}
