package user_joined_chat

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
	"skbt-pokur-bot/internal/core/events"
	"skbt-pokur-bot/internal/core/services"
)

type Handler struct {
	api      *tgbotapi.BotAPI
	services *services.Services
}

func New(api *tgbotapi.BotAPI, services *services.Services) *Handler {
	return &Handler{
		api:      api,
		services: services,
	}
}

func (h *Handler) Handle(event events.Event) error {
	usr, err := h.services.Users.GetOrCreate(event.User)
	if err != nil {
		return err
	}

	chat, err := h.services.Chats.GetOrCreate(event.Chat)
	if err != nil {
		return err
	}

	chat, err = h.services.Chats.AddUser(usr, chat)
	if err != nil {
		return err
	}

	log.Print("Пользователь присоединился к чату!")

	return nil
}
