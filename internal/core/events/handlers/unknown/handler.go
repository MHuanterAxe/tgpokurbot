package unknown

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"skbt-pokur-bot/internal/core/events"
	"skbt-pokur-bot/internal/core/keyboards"
)

type Handler struct {
	api *tgbotapi.BotAPI
}

func New(api *tgbotapi.BotAPI) *Handler {
	return &Handler{
		api: api,
	}
}

func (h *Handler) Handle(event events.Event) error {
	msg := tgbotapi.NewMessage(event.Chat.TgChatID, "Не понимаю тебя")

	msg.ReplyMarkup = keyboards.BaseKeyboard()

	_, err := h.api.Send(msg)
	if err != nil {
		return err
	}

	return nil
}
