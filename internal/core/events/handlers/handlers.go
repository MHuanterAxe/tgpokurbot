package handlers

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	chatschedules "skbt-pokur-bot/internal/core/events/handlers/chat-schedules"
	createunscheduledremind "skbt-pokur-bot/internal/core/events/handlers/create-unscheduled-remind"
	"skbt-pokur-bot/internal/core/events/handlers/unknown"
	userjoinedchat "skbt-pokur-bot/internal/core/events/handlers/user-joined-chat"
	usersubscribedforreminds "skbt-pokur-bot/internal/core/events/handlers/user-subscribed-for-reminds"
	userunsubscribedforreminds "skbt-pokur-bot/internal/core/events/handlers/user-unsubscribed-for-reminds"
	"skbt-pokur-bot/internal/core/services"
)

type Handlers struct {
	Unknown                    *unknown.Handler
	UserJoinedChat             *userjoinedchat.Handler
	ChatSchedules              *chatschedules.Handler
	CreateUnscheduledRemind    *createunscheduledremind.Handler
	UserSubscribedForReminds   *usersubscribedforreminds.Handler
	UserUnsubscribedForReminds *userunsubscribedforreminds.Handler
}

func New(api *tgbotapi.BotAPI, services *services.Services) *Handlers {
	return &Handlers{
		Unknown:                    unknown.New(api),
		UserJoinedChat:             userjoinedchat.New(api, services),
		CreateUnscheduledRemind:    createunscheduledremind.New(api, services),
		ChatSchedules:              chatschedules.New(api, services),
		UserSubscribedForReminds:   usersubscribedforreminds.New(api, services),
		UserUnsubscribedForReminds: userunsubscribedforreminds.New(api, services),
	}
}
