package user_subscribed_for_reminds

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
	"skbt-pokur-bot/internal/core/domain"
	"skbt-pokur-bot/internal/core/events"
	"skbt-pokur-bot/internal/core/keyboards"
	"skbt-pokur-bot/internal/core/services"
)

type Handler struct {
	api      *tgbotapi.BotAPI
	services *services.Services
}

func New(api *tgbotapi.BotAPI, services *services.Services) *Handler {
	return &Handler{
		api:      api,
		services: services,
	}
}

func (h *Handler) Handle(event events.Event) error {
	usr, err := h.services.Users.Get(event.User.TgUserID)
	if err != nil {
		return err
	}

	chat, err := h.services.Chats.Get(event.Chat.TgChatID)
	if err != nil {
		return err
	}

	hasUser, err := h.services.Chats.HasUser(chat, usr)

	if err != nil {
		return err
	}

	if hasUser {
		return h.deleteUser(chat, usr)
	}

	return h.noUser(chat, usr)
}

func (h *Handler) deleteUser(chat *domain.Chat, usr *domain.User) error {
	chat, err := h.services.Chats.DeleteUser(usr, chat)
	if err != nil {
		return err
	}

	log.Printf("Пользователь %s отписался от уведомлений!", usr.UserName)

	text := fmt.Sprintf("%s отключил уведомления в чате.\n", usr.FirstName)

	msg := tgbotapi.NewMessage(chat.TgChatID, text)

	msg.ReplyMarkup = keyboards.BaseKeyboard()

	_, err = h.api.Send(msg)
	if err != nil {
		return err
	}

	return nil
}

func (h *Handler) noUser(chat *domain.Chat, usr *domain.User) error {
	log.Printf("Пользователя %s нет в чате %d!", usr.UserName, chat.TgChatID)

	text := fmt.Sprintf("%s, ты еще не активировал уведомления в чате.\nЧтобы активировать их воспользуйся командой /subscribe", usr.FirstName)

	msg := tgbotapi.NewMessage(chat.TgChatID, text)

	msg.ReplyMarkup = keyboards.BaseKeyboard()

	_, err := h.api.Send(msg)
	if err != nil {
		return err
	}

	return nil
}
