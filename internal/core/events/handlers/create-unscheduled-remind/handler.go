package create_unscheduled_remind

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"skbt-pokur-bot/internal/core/events"
	"skbt-pokur-bot/internal/core/services"
	"time"
)

type Handler struct {
	api      *tgbotapi.BotAPI
	services *services.Services
}

func New(api *tgbotapi.BotAPI, services *services.Services) *Handler {
	return &Handler{
		api:      api,
		services: services,
	}
}

func (h *Handler) Handle(event events.Event) error {
	usr, err := h.services.Users.Get(event.User.TgUserID)
	if err != nil {
		return err
	}

	chat, err := h.services.Chats.Get(event.Chat.TgChatID)
	if err != nil {
		return err
	}

	remind, err := h.services.Reminds.Create(chat.ID, usr.ID, time.Now().Add(time.Minute*5).UTC())
	if err != nil {
		return err
	}

	remind, err = h.services.Reminds.Subscribe(remind.ID, remind.ChatID, remind.CreatorID)
	if err != nil {
		return err
	}

	t := remind.Time.Local().Format("15:04")
	text := fmt.Sprintf("Покур запланирован на %s", t)

	msg := tgbotapi.NewMessage(event.Chat.TgChatID, text)

	msg.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Иду", "true"),
			tgbotapi.NewInlineKeyboardButtonData("Не иду", "false"),
		),
	)

	_, err = h.api.Send(msg)
	if err != nil {
		return err
	}

	return nil
}
