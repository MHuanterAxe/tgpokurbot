package chat_schedules

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
	"math"
	"skbt-pokur-bot/internal/core/domain"
	"skbt-pokur-bot/internal/core/events"
	"skbt-pokur-bot/internal/core/keyboards"
	"skbt-pokur-bot/internal/core/services"
	"strings"
	"time"
)

type Handler struct {
	api      *tgbotapi.BotAPI
	services *services.Services
}

func New(api *tgbotapi.BotAPI, services *services.Services) *Handler {
	return &Handler{
		api:      api,
		services: services,
	}
}

func (h *Handler) Handle(event events.Event) error {
	log.Printf("Пользователь %s запросил напоиманиня в чате %d!", event.User.UserName, event.Chat.TgChatID)

	d := time.Now()

	reminds, err := h.services.Reminds.All(event.Chat, d)
	if err != nil {
		return err
	}

	if len(reminds) == 0 {
		return h.noReminds(event)
	}

	return h.showReminds(event, reminds, d)
}

func kek(a *domain.Chat) {
	log.Printf("%v", a)
}

func (h *Handler) noReminds(event events.Event) error {
	text := "Сегодня нет ни одного запланированного покура"

	msg := tgbotapi.NewMessage(event.Chat.TgChatID, text)

	msg.ReplyMarkup = keyboards.BaseKeyboard()

	_, err := h.api.Send(msg)
	if err != nil {
		return err
	}

	return nil
}

func (h *Handler) showReminds(event events.Event, reminds []*domain.Remind, date time.Time) error {
	t := date.Format("2\\.01\\.2006")
	text := fmt.Sprintf("Список покуров на *%s*:\n\n", t)

	rs := make([]string, len(reminds))

	for idx, remind := range reminds {
		t = remind.Time.Local().Format("15:04")
		rs[idx] = fmt.Sprintf("%d\\. *%s*%s", idx+1, t, h.checkExpired(remind.Time, date))
	}

	text += strings.Join(rs, "\n")

	msg := tgbotapi.NewMessage(event.Chat.TgChatID, text)

	msg.ParseMode = "MarkdownV2"

	msg.ReplyMarkup = keyboards.BaseKeyboard()

	_, err := h.api.Send(msg)
	if err != nil {
		return err
	}

	return nil
}

func (h *Handler) checkExpired(remindTime time.Time, currentTime time.Time) string {
	c := currentTime.UTC()

	if remindTime.Before(c) {
		return " \\- прошел"
	}

	t := remindTime.Sub(c)

	if t.Hours() < 1 {
		return fmt.Sprintf(" \\- через %.f минут", math.Round(t.Minutes()))
	}

	return ""
}
