package event_listener

import (
	"context"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
	"skbt-pokur-bot/internal/core/events"
	event_processor "skbt-pokur-bot/internal/core/events/event-processor"
	event_transformer "skbt-pokur-bot/internal/core/events/event-transformer"
	"skbt-pokur-bot/internal/core/services"
)

type eventListener struct {
	transformer events.EventTransformer
	processor   events.EventProcessor
}

func New(api *tgbotapi.BotAPI, services *services.Services) *eventListener {
	processor := event_processor.New(api, services)

	return &eventListener{
		transformer: event_transformer.New(processor),
		processor:   processor,
	}
}

func (l *eventListener) Listen(ctx context.Context, updChan tgbotapi.UpdatesChannel) error {
	log.SetPrefix("[Listener] ")

	for {
		select {
		case upd := <-updChan:
			go func() {
				event, err := l.transformer.Transform(upd)

				if err != nil {
					log.Printf("%v\n", err)
				}

				err = l.processor.Process(event)

				if err != nil {
					log.Printf("%s\n", err)
				}
			}()
		case <-ctx.Done():
			return nil
		}
	}
}
