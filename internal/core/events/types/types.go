package types

type EventType string

const (
	ChatSchedules           EventType = "chat_schedules"
	CreateUnscheduledRemind EventType = "create_unscheduled_remind"
	Unknown                 EventType = "unknown"
)

type CommandType EventType

const (
	UserSubscribedForReminds   EventType = "user_subscribed_for_reminds"
	UserUnsubscribedForReminds EventType = "user_unsubscribed_for_reminds"
)
