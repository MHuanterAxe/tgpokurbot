package commands

type Command string

const (
	SubscribeForRemindsInChat   Command = "subscribe"
	UnsubscribeForRemindsInChat Command = "unsubscribe"
)
