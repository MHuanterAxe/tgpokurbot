package bot

import (
	"context"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/joho/godotenv"
	"log"
	"os"
	"skbt-pokur-bot/db"
	event_listener "skbt-pokur-bot/internal/core/events/event-listener"
	"skbt-pokur-bot/internal/core/services"
)

type bot struct {
}

func New() *bot {
	return &bot{}
}

func (b *bot) Start() {
	godotenv.Load()
	token := mustToken()
	ctx := context.Background()

	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		panic(err)
	}

	fmt.Println(fmt.Sprintf("Авторизован, как %s", bot.Self.FirstName))

	bot.Debug = true

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	l := event_listener.New(bot, services.New(ctx, db.New()))

	updates := bot.GetUpdatesChan(u)

	err = l.Listen(ctx, updates)
	if err != nil {
		log.Print(err)
	}
}

func mustToken() string {
	return os.Getenv("TELEGRAM_API_TOKEN")
}
