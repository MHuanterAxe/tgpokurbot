package keyboards

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

func BaseKeyboard() tgbotapi.ReplyKeyboardMarkup {
	return tgbotapi.NewReplyKeyboard(
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton("Создать напоминание"),
			tgbotapi.NewKeyboardButton("Предстоящие покуры"),
		),
	)
}
